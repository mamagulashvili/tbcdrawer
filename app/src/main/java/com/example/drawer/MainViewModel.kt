package com.example.drawer

import android.util.Log.d
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.drawer.api.RetrofitService
import kotlinx.coroutines.*

class MainViewModel : ViewModel() {
    var job: Job? = null
    fun sendRequest() {
        job = viewModelScope.launch(Dispatchers.IO) {
            while (isActive) {
                val response = RetrofitService.api.getItem()
                if (response.isSuccessful) {
                    d("RESPONSE", "${response.body()}")
                } else {
                    !isActive
                    d("RESPONSE", "${response.errorBody()}")
                }
                delay(5000)
            }
        }

    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }
}