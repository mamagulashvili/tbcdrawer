package com.example.drawer.model

data class DrawerItemModel(
    val icon:Int,
    val name:String
)
