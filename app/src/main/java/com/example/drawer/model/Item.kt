package com.example.drawer.model

data class Item(
    val fileSizeBytes:Int,
    val url :String
)
