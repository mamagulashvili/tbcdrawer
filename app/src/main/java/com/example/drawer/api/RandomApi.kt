package com.example.drawer.api

import com.example.drawer.model.Item
import retrofit2.Response
import retrofit2.http.GET

interface RandomApi {
    @GET("/breeds/image/random")
    suspend fun getItem():Response<List<Item>>
}