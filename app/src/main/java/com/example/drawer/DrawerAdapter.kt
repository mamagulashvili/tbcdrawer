package com.example.drawer

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.example.drawer.databinding.RowDrawerItemBinding
import com.example.drawer.model.DrawerItemModel

typealias onMenuItemClick = (position: Int) -> Unit

class DrawerAdapter : RecyclerView.Adapter<DrawerAdapter.DrawerViewHolder>() {

    private val itemList = mutableListOf<DrawerItemModel>()
    lateinit var onMenuItemClick: onMenuItemClick
    var selectedPosition: Int = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrawerViewHolder =
        DrawerViewHolder(
            RowDrawerItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: DrawerViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int = itemList.size

    inner class DrawerViewHolder(val binding: RowDrawerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var menuItem: DrawerItemModel
        fun onBind() {
            menuItem = itemList[adapterPosition]
            binding.apply {
                root.setOnClickListener {
                    selectedPosition = adapterPosition
                    onMenuItemClick.invoke(adapterPosition)
                    notifyDataSetChanged()
                }
                drawerTv.text = menuItem.name
                drawerIcon.setImageResource(menuItem.icon)
            }
            if (adapterPosition == selectedPosition) {
                binding.itemIndicator.isVisible = true
                binding.root.setBackgroundColor(
                    ContextCompat.getColor(
                        binding.root.context,
                        R.color.teal_700
                    )
                )
                binding.itemIndicator.setBackgroundColor(
                    ContextCompat.getColor(
                        binding.root.context,
                        R.color.black
                    )
                )
            } else {
                binding.root.setBackgroundColor(
                    ContextCompat.getColor(
                        binding.root.context,
                        android.R.color.transparent
                    )
                )
                binding.itemIndicator.isVisible = false
            }
        }
    }

    fun setData(newList: MutableList<DrawerItemModel>) {
        this.itemList.apply {
            clear()
            addAll(newList)
        }
        notifyDataSetChanged()
    }
}