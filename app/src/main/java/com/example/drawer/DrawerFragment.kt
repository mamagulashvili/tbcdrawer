package com.example.drawer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.drawer.databinding.FragmentDrawerBinding
import com.example.drawer.model.DrawerItemModel
import kotlinx.coroutines.launch

class DrawerFragment : Fragment() {
    private var _binding: FragmentDrawerBinding? = null
    private val binding get() = _binding!!
    private val drawerAdapter: DrawerAdapter by lazy { DrawerAdapter() }
    private val viewModel: MainViewModel by viewModels()

    private lateinit var toggle:ActionBarDrawerToggle
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDrawerBinding.inflate(layoutInflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        setRecycle()
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.sendRequest()
        }
        toggle = ActionBarDrawerToggle(requireActivity(),binding.root,R.string.open,R.string.close)
        binding.root.addDrawerListener(toggle)
        toggle.syncState()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item))
            return true
        return super.onOptionsItemSelected(item)
    }

    private fun setRecycle() {
        binding.rvDrawer.apply {
            layoutManager = LinearLayoutManager(requireContext())
            clipToPadding = false
            adapter = drawerAdapter
        }
        setData()

    }

    private fun setData() {
        val itemList = mutableListOf<DrawerItemModel>()
        itemList.apply {
            add(DrawerItemModel(R.drawable.ic_cake, "Cake"))
            add(DrawerItemModel(R.drawable.ic_camera, "Camera"))
            add(DrawerItemModel(R.drawable.ic_covid, "Covid"))
        }
        drawerAdapter.onMenuItemClick = {
            when (it) {
                0 -> {
                    findNavController().navigate(R.id.action_drawerFragment_to_firstFragment)
                }
                1 -> {
                    findNavController().navigate(R.id.action_drawerFragment_to_secondFragment)
                }
                2 -> {
                    findNavController().navigate(R.id.action_drawerFragment_to_thirdFragment)
                }
            }
        }
        drawerAdapter.setData(itemList)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}